*********************************************************
*														*
*	FACULDADE DE ENGENHARIA DA UNIVERSIDADE DO PORTO	*
*														*
*				MIEIC	-	DJCO 2015					*
*														*
*********************************************************

*********************************************************
				CHEMISTRY FROM HELL
*********************************************************

Grupo3:
	Luis Abreu
	Luis Araujo
	Joao Pinto
	Pedro Oliveira
	
*********************************************************


SOURCE CODE: https://www.dropbox.com/s/rtdot0funx18g0r/DJCO-P1-G3-ChemistryFromHell-source.zip?dl=0

GAME BUILD: https://www.dropbox.com/s/e6m3cctxytd2f5r/DJCO-P1-G3-ChemistryFromHell-game.zip?dl=0

VIDEO PREVIEW: https://www.dropbox.com/s/nr6e0bmq7ozezbk/game_preview.mov?dl=0