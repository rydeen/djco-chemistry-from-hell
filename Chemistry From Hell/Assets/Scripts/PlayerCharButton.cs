﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to control the left buttons available for the player to buy new characters
 */

public class PlayerCharButton : MonoBehaviour {

	public int charIndex ;
	public bool active ;
	private float myPrice ;

	public Sprite ActiveSprite ;
	public Sprite DesactiveSprite ;
	public Sprite SelectedSprite ;

	private SetPlayerCharacter set_script ;
	private PlayerScore score_script ;

	void Start()
	{
		this.set_script = GameObject.Find ("GameLogic").GetComponent<SetPlayerCharacter> ();
		this.score_script = GameObject.Find ("GameLogic").GetComponent<PlayerScore> ();

		this.myPrice = this.set_script.prices [this.charIndex];
	}

	void Update()
	{
		if (this.myPrice > this.score_script.score) 
		{
			this.active = false ;
			this.GetComponent<SpriteRenderer>().sprite = this.DesactiveSprite ;
		} 
		else 
		{
			this.active = true ;

			if ( this.set_script.isSelected == true && this.set_script.index == this.charIndex )
				this.GetComponent<SpriteRenderer>().sprite = this.SelectedSprite ;
			else
				this.GetComponent<SpriteRenderer>().sprite = this.ActiveSprite ;
		}
	}

	void OnMouseDown()
	{
		if (this.active) 
		{
			if ( this.set_script.isSelected == true && this.set_script.index == this.charIndex )
			{
				this.set_script.isSelected = false ;
			}
			else
			{
				this.set_script.isSelected = true ;
				this.set_script.index = this.charIndex ;
			}
		}
	}
}
