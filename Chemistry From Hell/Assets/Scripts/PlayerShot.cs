﻿using UnityEngine;
using System.Collections;

/*
 *	This script controls the projectile shoot by a playable character
 */

public class PlayerShot : MonoBehaviour {

	public float movementSpeed ;
	public float damage ;

	private Vector3 direction ;

	void Start()
	{
		this.direction = new Vector3 (1, 0, 0);
	}

	void Update () {

		// projectile movement
		transform.Translate ( this.direction * this.movementSpeed * Time.deltaTime);
	}

	void OnCollisionEnter2D( Collision2D collision )
	{
		if ( collision.collider.tag == "Tile" || collision.collider.tag == "Player" || collision.collider.tag == "Player Shot" )
			Physics2D.IgnoreCollision( collision.collider , this.gameObject.GetComponent<BoxCollider2D>() ) ;
	}
}
