﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to manage the waves of enemies
 */

public class EnemyWaveManager : MonoBehaviour {


	public int numEnemiesSpawned ;
	public int maxNumEnemiesToSpawn;

	public GameObject[] enemiesToSpaw ;
	private int indexSpawn ;

	public float cooldownSpawn ;
	private float cooldownCounter ;

	private float[] yPos ; // y positions to spawn an enemy on any of the rows
	private float yFirst ; // value of y for the bottom row
	private float yInc ; // value of y between rows

	// Use this for initialization
	void Start () 
	{
		this.cooldownCounter = this.cooldownSpawn;
		this.yFirst = 6.9f ;
		this.yInc = 9.6f ;

		this.yPos = new float[4] ;

		for (int i=0; i<this.yPos.Length; i++) 
		{
			if ( i == 0 )
				this.yPos[i] = this.yFirst ;
			else
				this.yPos[i] = this.yPos[i-1] + this.yInc ;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (this.numEnemiesSpawned < this.maxNumEnemiesToSpawn) 
		{
			if (this.cooldownCounter > 0) 
			{
				this.cooldownCounter -= Time.deltaTime;
			} 
			else 
			{
				if ( this.numEnemiesSpawned < this.maxNumEnemiesToSpawn/4 ) // first quarter of the wave..
				{
					this.indexSpawn = Random.Range (0, 1); // only simlpe enemies
				}
				else if ( this.numEnemiesSpawned < this.maxNumEnemiesToSpawn/2 ) // second quarter of the wave..
				{
					this.indexSpawn = Random.Range (0, 2); // normal and shield enemies
				}
				else // third and fourth quarter of the wave..
				{
					this.indexSpawn = Random.Range (0, 3); // craw, nomal and shield enemies
				}

				//this.indexSpawn = Random.Range (0, this.enemiesToSpaw.Length);

				int randomY = Random.Range (0, this.yPos.Length);
				Vector3 pos = new Vector3 (80f, this.yPos [randomY], -39f);

				Instantiate (this.enemiesToSpaw [this.indexSpawn], pos, Quaternion.identity);

				this.numEnemiesSpawned++;

				this.cooldownCounter = this.cooldownSpawn;
			}
		}
		else // if the wave has ended, find out when the game ends
		{
			int enemiesLeft = GameObject.FindGameObjectsWithTag("Enemy").Length;
			if ( enemiesLeft == 0 )
			{
				GameObject.Find ("GameLogic").GetComponent<EndGame>().won = true ;
				GameObject.Find ("GameLogic").GetComponent<EndGame>().end = true ;
			}
		}
	}
}
