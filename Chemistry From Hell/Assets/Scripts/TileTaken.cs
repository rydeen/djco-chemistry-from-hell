﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to keep informations about a single board tile
 */

public class TileTaken : MonoBehaviour {

	// identifies if a tile has already been occupied
	public bool isTaken ;

	// identifies the object that is occupying a board tile if not taken
	public GameObject PLayersCharacter ;

	public SetPlayerCharacter set_script ;

	public int yTilePos ;
	public int xTilePos ;

	private BoardPos playerPos ;

	void Start()
	{
		this.set_script = GameObject.Find ("GameLogic").GetComponent<SetPlayerCharacter> ();
	}

	void OnMouseDown()
	{

		if (! this.isTaken) 
		{
			// if a tile was not taken, and a character has been selected...
			if ( this.set_script.isSelected )
			{
				this.playerPos = GameObject.Find ("GameLogic").GetComponent<BoardPositions> ().boardXYpos [this.xTilePos, this.yTilePos];
				PlayerScore score_script = GameObject.Find ("GameLogic").GetComponent<PlayerScore> ();
				score_script.score -= this.set_script.prices[this.set_script.index] ;
				this.isTaken = true ;
				this.set_script.isSelected = false ;

				Vector3 pos = new Vector3( this.playerPos.xPos, this.playerPos.yPos , this.playerPos.zPos ) ;

				this.PLayersCharacter = (GameObject)Instantiate( this.set_script.characters[this.set_script.index] , pos , Quaternion.identity ) ;
			}
		} 
		else if ( this.set_script.isSelected )
		{
			this.set_script.isSelected = false ; // you have to select the character to purchase again..
		}
	}

	// when an enemy kills a player, the tile becomes available again
	void ResetTile( GameObject player )
	{
		if (this.isTaken && this.PLayersCharacter == player) 
		{
			this.isTaken = false ;
			this.PLayersCharacter = null ;
		}
	}
}
