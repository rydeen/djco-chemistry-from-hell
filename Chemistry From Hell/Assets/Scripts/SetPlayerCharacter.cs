﻿using UnityEngine;
using System.Collections;

/*
 * This script is used to manage the characters available and their prices, and to control the button selected
 */

public class SetPlayerCharacter : MonoBehaviour {


	public bool isSelected ; // indicates if a type of character was selected by the player with the mouse
	public int index ; // index of the character selected corresponding to a character in array charachters

	public GameObject[] characters ; // array of available characters to purchase
	public float[] prices ; // array of prices corresponding to each character in characters array

}
