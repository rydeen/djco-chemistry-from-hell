﻿using UnityEngine;
using System.Collections;

public class LoadLevelOnClick : MonoBehaviour {
	/*
	// Use this for initialization
	void Start () {
	
	}
	*/
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Escape))
			QuitGame ();
	}


	public void LoadScene(int level) {
		Application.LoadLevel (level);
	}

	public void QuitGame() {
		Application.Quit();
	}
}
