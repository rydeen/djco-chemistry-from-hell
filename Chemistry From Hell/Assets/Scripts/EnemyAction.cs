﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to control an enemy attack to a player's character
 */

public class EnemyAction : MonoBehaviour {

	private EnemyStatistics enemyStats_script ;
	public EnemyBehavior enemyMovement_script ;
	public bool dealtDamage ;
	public Collider2D playersCharacter ;
	public float cooldown ;
	public float cooldownTimer ;

	// Use this for initialization
	void Start () {
		this.enemyStats_script = gameObject.GetComponent<EnemyStatistics> ();
		this.enemyMovement_script = gameObject.GetComponentInParent<EnemyBehavior> ();
		this.dealtDamage = false;
		this.cooldownTimer = 0;
		this.playersCharacter = null;
	}
	
	// Update is called once per frame
	void Update () {

		if (this.dealtDamage) // reached a player's character
		{
			if( this.cooldownTimer > 0 ) // waiting ...
			{
				this.cooldownTimer -= Time.deltaTime ;
			} 
			else // attacking the player's character !
			{
				if( this.playersCharacter != null ) 
				{
					if ( this.playersCharacter.gameObject.GetComponent<Health>().health - this.enemyStats_script.damageDealt <= 0.0 ) // if the player's character is going to die..
					{
						playersCharacter.gameObject.GetComponent<Health>().health -=
							this.enemyStats_script.damageDealt ; // kill him ..
						this.playersCharacter = null ; // forget the collider ..
						this.dealtDamage = false ; // stop dealing damage ..
						this.enemyMovement_script.canMove = true ; // start to move again ..

						//this.GetComponent<Animator>().enabled = true ; // start the animation again ..
						this.GetComponent<Animator>().SetBool ("move", true) ;
						this.GetComponent<Animator>().SetBool ("attack", false) ;
					}
					else
					{
						this.playersCharacter.gameObject.GetComponent<Health>().health -=
							this.enemyStats_script.damageDealt ;
					}
				}
				else
				{
					this.GetComponent<Animator>().SetBool ("move", true) ;
					this.GetComponent<Animator>().SetBool ("attack", false) ;
				}

				this.cooldownTimer = this.cooldown ;
			}
		}

	}

}
