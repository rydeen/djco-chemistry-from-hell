﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to check the boundaries of the board.
 *		- destroy player's projectiles when they reach the end of the board
 *		- stop the game if an enemy has reached the "FEUP" (the protected house)
 */
public class RowTrigger : MonoBehaviour {

	void OnTriggerEnter2D( Collider2D collider ) {

		if (collider.tag == "Enemy") 	// enemies can only collide when they reached "FEUP"
		{ 								// because they can only collide with the left end of the board

			// YOU LOST THE GAME....

			collider.gameObject.GetComponentInParent<EnemyBehavior>().canMove = false ;
			GameObject.Find ("GameLogic").GetComponent<EndGame>().won = false ;
			GameObject.Find ("GameLogic").GetComponent<EndGame>().end = true ;
		} 
		else if (collider.tag == "Player Shot") // player's projectiles can only collide 
		{										// with the right end of the board
			Destroy (collider.gameObject ) ;
		}

	}
}
