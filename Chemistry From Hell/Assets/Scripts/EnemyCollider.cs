﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to detect the collisions within the board:
 *		- enemy vs player
 *		- enemy vs player's projectile
 */

public class EnemyCollider : MonoBehaviour {

	public EnemyBehavior enemyMovement_script ;
	public EnemyAction enemyAction_script ;

	public bool isDown ;

	void Start() {
		this.enemyMovement_script = gameObject.GetComponent<EnemyBehavior> ();
		this.enemyAction_script = gameObject.GetComponent<EnemyAction> ();

	}
	
	void OnCollisionEnter2D( Collision2D collision )
	{
		if ( this.isDown == false ) 
		{
			// verify if the enemy collided with ...
			if (collision.collider.tag == "Player") {  // ... a player 
				//Debug.Log ("Found a player") ;

				// stop the enemy's movement
				this.enemyMovement_script.canMove = false;
				// dealt damage to the player
				this.enemyAction_script.dealtDamage = true;
				this.enemyAction_script.playersCharacter = collision.collider;

				this.GetComponent<Animator> ().SetBool ("move", false);
				this.GetComponent<Animator> ().SetBool ("attack", true);
			} 
			else if (collision.collider.tag == "Player Shot" || collision.collider.tag == "Player Shot Down") { // ... or a projectile 
				// dealt damage to the enemy
				gameObject.GetComponentInParent<Health> ().health -= collision.collider.gameObject.GetComponent<PlayerShot> ().damage;
				// and destroy the projectile
				Destroy (collision.collider.gameObject);
			} 
			else if (collision.collider.tag == "Tile" || collision.collider.tag == "Enemy") {
				Physics2D.IgnoreCollision (collision.collider, this.gameObject.GetComponent<BoxCollider2D> ());
			}
		}
		else if ( this.isDown == true )
		{
			// verify if the enemy collided with ...
			if (collision.collider.tag == "Player") {  // ... a player 
				//Debug.Log ("Found a player") ;
				
				// stop the enemy's movement
				this.enemyMovement_script.canMove = false;
				// dealt damage to the player
				this.enemyAction_script.dealtDamage = true;
				this.enemyAction_script.playersCharacter = collision.collider;
				
				this.GetComponent<Animator> ().SetBool ("move", false);
				this.GetComponent<Animator> ().SetBool ("attack", true);
			} 
			else if (collision.collider.tag == "Player Shot Down") { // ... or a projectile 
				// dealt damage to the enemy
				gameObject.GetComponentInParent<Health> ().health -= collision.collider.gameObject.GetComponent<PlayerShot> ().damage;
				// and destroy the projectile
				Destroy (collision.collider.gameObject);
			} 
			else if (collision.collider.tag == "Tile" || collision.collider.tag == "Enemy" || collision.collider.tag == "Player Shot") {
				Physics2D.IgnoreCollision (collision.collider, this.gameObject.GetComponent<BoxCollider2D> ());
			}
		}
	}

}
