﻿using UnityEngine;
using System.Collections;

/*
 *	This script is responsable for spawning the player's projectiles
 */


public class PlayerAction : MonoBehaviour {

	public float cooldown ; // default time between each projectile
	public float cooldownTimer ; // time left until the next projectile
	public GameObject projectile ; 
	private LayerMask enemiesLayer;

	public bool isUp ;

	private EraserScript eraser_script ;

	public BoardPos projectileInitialPos ;

	// Use this for initialization
	void Start () {
		this.cooldownTimer = this.cooldown;
		this.enemiesLayer =  1 << 10 ; // layer name = "Enemies"
		this.eraser_script = GameObject.Find ("Button_eraser").GetComponent<EraserScript> ();

		if ( this.isUp )
			this.projectileInitialPos = new BoardPos (transform.position.x, transform.position.y);
		else
			this.projectileInitialPos = new BoardPos (transform.position.x, transform.position.y-2.0f );
	}
	
	// Update is called once per frame
	void Update () {
	
		if (this.cooldownTimer > 0) // wainting...
		{
			this.cooldownTimer -= Time.deltaTime;
		} 
		else // time to kill...
		{
			RaycastHit2D enemySpotted = Physics2D.Raycast(transform.position, Vector2.right, Mathf.Infinity , this.enemiesLayer );

			if ( enemySpotted ) // if there is an enemy in front, kill it
			{
				bool shotDown = enemySpotted.collider.gameObject.GetComponent<EnemyCollider>().isDown ;

				if( (shotDown == true && this.isUp==false) || shotDown == false ) // if the enemy is crawling...
				{
					this.GetComponent<Animator>().SetBool ("attack", true) ;
					Instantiate( this.projectile , this.projectileInitialPos.getVector3() , Quaternion.identity) ;
					this.cooldownTimer = this.cooldown ; // resart cooldown
				}
				else
					this.GetComponent<Animator>().SetBool ("attack", false) ;
			}
			else
			{
				this.GetComponent<Animator>().SetBool ("attack", false) ;
			}
		}

	}

	void OnCollisionEnter2D ( Collision2D collision )
	{
		if ( collision.collider.tag == "Tile" || collision.collider.tag == "Player Shot" ) 
			Physics2D.IgnoreCollision ( collision.collider , this.GetComponent<BoxCollider2D> ()) ;
	}

	void OnMouseDown()
	{
		if ( this.eraser_script.activeEraser )
		{
			this.gameObject.GetComponent<Health>().health = 0 ;
			this.eraser_script.activeEraser = false ;
			this.eraser_script.GetComponent<SpriteRenderer>().sprite = this.eraser_script.ActiveSprite ;
		}
	}
}
