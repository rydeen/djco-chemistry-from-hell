﻿using UnityEngine;
using System.Collections;

public class PlayerShield : MonoBehaviour {

	private EraserScript eraser_script ;

	// Use this for initialization
	void Start () {
		this.eraser_script = GameObject.Find ("Button_eraser").GetComponent<EraserScript> ();
	}

	void OnCollisionEnter2D ( Collision2D collision )
	{
		if ( collision.collider.tag == "Tile" ) 
			Physics2D.IgnoreCollision ( collision.collider , this.GetComponent<BoxCollider2D> ()) ;
	}

	void OnMouseDown()
	{
		if ( this.eraser_script.activeEraser )
		{
			this.gameObject.GetComponent<Health>().health = 0 ;
			this.eraser_script.activeEraser = false ;
			this.eraser_script.GetComponent<SpriteRenderer>().sprite = this.eraser_script.ActiveSprite ;
		}
	}
}
