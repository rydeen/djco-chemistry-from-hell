﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *	This script is used to keep track of the actual score in the game
 *		-	this script is part of an empty GameObject that is intended to manage 
 *			this kind of Game Logic variables
 */

public class PlayerScore : MonoBehaviour {

	// player's score
	public float score ;
	public Text scoreText ;

	//private GUIStyle labelStyle ;

	void Start()
	{
		//this.labelStyle.fontSize = 4000;
	}

	void Update()
	{
		this.scoreText.text = "Score: " + score;
	}


	/*void OnGUI() 
	{
		GUI.Label (new Rect (10, 10, 100, 20), "Score: "+this.score );
	}*/
	
}
