﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to manage the collisions and character's death
 */

public class Death : MonoBehaviour {

	public bool isEnemie ;
	private Health health_script ;
	private PlayerScore playerScore_script ;
	private EnemyStatistics enemyStats_script ;

	public float enemyDeathAnimationTime ;

	private bool rewarded ;

	// Use this for initialization
	void Start () {
		this.health_script = gameObject.GetComponent<Health> ();
		this.playerScore_script = GameObject.Find ("GameLogic").GetComponent<PlayerScore>();

		this.rewarded = false;

		if (this.isEnemie) // if it's an enemy it has a stats script
		{
			this.enemyStats_script = gameObject.GetComponent<EnemyStatistics> () ;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (this.health_script.health <= 0) // if a character is dead...
		{
			if( this.isEnemie ) // it can be an enemy
			{
				// give points/money to the player for this kill
				if ( ! this.rewarded )
				{
					this.playerScore_script.score += this.enemyStats_script.worthScore ;
					this.rewarded = true ;
				}

				this.gameObject.GetComponent<Animator> ().SetBool("dead", true);

				StartCoroutine ( KillOnAnimationEnd() );

				//Destroy(gameObject) ;
			}
			else // or it can be a player's character
			{
				NotifyDeath() ; // we need to notify the tile so it becomes available again
				Destroy(gameObject) ;
			}
		}
	}

	// send a message to all tiles to "Reset" if they have a reference to this.gameObject
	void NotifyDeath()
	{
		GameObject[] tiles = GameObject.FindGameObjectsWithTag ("Tile");
		for (int i=0; i<tiles.Length; i++)
			tiles [i].SendMessage ("ResetTile", this.gameObject);
	}

	private IEnumerator KillOnAnimationEnd()
	{
		yield return new WaitForSeconds( this.enemyDeathAnimationTime ) ;
		Destroy (gameObject);
	}
}
