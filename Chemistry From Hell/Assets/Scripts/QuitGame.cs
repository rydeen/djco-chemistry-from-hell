﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour {

	void OnMouseDown()
	{
		GameObject.Find ("GameLogic").GetComponent<EndGame> ().gameObject.SendMessage ("Quit");
	}
}
