﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to control the enemies movement
 */

public class EnemyBehavior : MonoBehaviour {
	
	public float movementSpeed ;
	public bool canMove ;

	private Rigidbody2D body ;
	private Vector2 pos ;

	// Use this for initialization
	void Start () {
		this.canMove = true;
		this.body = this.GetComponent<Rigidbody2D> ();
		this.pos = new Vector2 ( transform.position.x, transform.position.y );
	}
	
	// Update is called once per frame
	void Update () {
		if (this.canMove) // enemy's movement...
		{
			//transform.Translate (Vector3.left * movementSpeed * Time.deltaTime);
			this.body.rotation = 0 ;
			this.pos.x -= movementSpeed*Time.deltaTime ;
			this.body.MovePosition( pos ) ;
		}
	}
	
}