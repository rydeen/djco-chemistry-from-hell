﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to control the "erase" button available on the left of the screen
 */


public class EraserScript : MonoBehaviour {

	public bool activeEraser ;

	public Sprite ActiveSprite ;
	public Sprite SelectedSprite ;

	void OnMouseDown()
	{
		if (this.activeEraser) 
		{
			this.activeEraser = false;
			this.GetComponent<SpriteRenderer>().sprite = this.ActiveSprite ;
		}
		else 
		{
			this.activeEraser = true;
			this.GetComponent<SpriteRenderer>().sprite = this.SelectedSprite ;
		}
	}
}
