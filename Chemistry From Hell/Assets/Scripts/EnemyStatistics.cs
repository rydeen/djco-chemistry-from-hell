﻿using UnityEngine;
using System.Collections;

/*
 *	This script is used to store enemies values
 */

public class EnemyStatistics : MonoBehaviour {
	
	public float worthScore ; // score earned by the player when it kills an enemy
	public float damageDealt ; // damage dealt to a player's character when an enemy is attacking
}
