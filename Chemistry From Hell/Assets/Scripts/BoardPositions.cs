﻿using UnityEngine;
using System.Collections;

public class BoardPositions : MonoBehaviour {

	public BoardPos[,] boardXYpos ;

	private float[] yPos ;
	private float yFirst ;
	private float yInc ;

	private float[] xPos ;
	private float xFirst ;
	private float xInc ;

	void Start()
	{
		// -------------------------------------- yy
		this.yPos = new float[4] ;
		this.yFirst = 6.9f ;
		this.yInc = 9.6f ;
		
		for (int i=0; i<this.yPos.Length; i++) 
		{
			if ( i == 0 )
				this.yPos[i] = this.yFirst ;
			else
				this.yPos[i] = this.yPos[i-1] + this.yInc ;
		}

		// -------------------------------------- xx
		this.xPos = new float[6] ;
		this.xFirst = 25.2f ;
		this.xInc = 9.6f ;

		for (int i=0; i<this.xPos.Length; i++) 
		{
			if ( i == 0 )
				this.xPos[i] = this.xFirst ;
			else
				this.xPos[i] = this.xPos[i-1] + this.xInc ;
		}

		// -------------------------------------- board xxyy
		this.boardXYpos = new BoardPos[this.xPos.Length,this.yPos.Length] ; // 6x and 5y :: the board dimensions
		BoardPos auxPos;
		for (int i=0; i<this.xPos.Length; i ++) 
		{
			for (int j=0; j<this.yPos.Length; j++) 
			{
				auxPos = new BoardPos( this.xPos[i] , this.yPos[j] ) ;
				this.boardXYpos[i,j] = auxPos ;
			}
		}

		//PrintBoardPos ();
	}

	void PrintBoardPos()
	{
		for (int i=0; i<this.xPos.Length; i ++)
			for (int j=0; j<this.yPos.Length; j++) 
				Debug.Log ( "(" + i + "," + j + ") = " + "(" + this.boardXYpos[i,j].xPos + "," + this.boardXYpos[i,j].yPos + ")" );
	}
}
