﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/*
 *	This script controls the end of the game, if the player has won or lost
 */
public class EndGame : MonoBehaviour {


	public bool end ;
	public bool won ;
	private float initialScore ;
	private float inicialCooldown ;

	public Text displayText ;
	private string won_msg = "YES! YOU KILL THEM ALL!\nYOU WON!" ;
	private string lost_msg = "OH NO! FEUP IS DOOMED!\nYOU LOST!" ;

	// Use this for initialization
	void Start () {
		this.end = false;
		this.won = false;
		this.initialScore = this.gameObject.GetComponent<PlayerScore> ().score;
		this.inicialCooldown = this.gameObject.GetComponent<EnemyWaveManager> ().cooldownSpawn;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (this.end) // if the game ended...
		{
			GameObject[] players = GameObject.FindGameObjectsWithTag("Player") ;
			GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy") ;

			for( int i=0 ; i<players.Length ; i++ )
				players[i].GetComponent<Animator>().enabled = false ;

			for( int j=0 ; j<enemies.Length ; j++ )
			{
				enemies[j].GetComponent<Animator>().enabled = false ;
				enemies[j].GetComponent<EnemyBehavior>().canMove = false ;
			}


			if( this.won )
			{
				//Debug.Log ("YOU WON!");
				this.displayText.text = this.won_msg ;
			}
			else
			{
				this.displayText.text = this.lost_msg ;
				//Debug.Log ("YOU LOST!");
			}

		}
	}

	void Quit()
	{
		Application.LoadLevel (0);
	}

	void Resart()
	{
		GameObject[] players = GameObject.FindGameObjectsWithTag("Player") ;
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy") ;
		GameObject[] projectiles = GameObject.FindGameObjectsWithTag("Player Shot") ;
		GameObject[] projectiles2 = GameObject.FindGameObjectsWithTag("Player Shot Down") ;
		GameObject[] tiles = GameObject.FindGameObjectsWithTag("Tile") ;
		GameObject eraseButton = GameObject.FindGameObjectWithTag("EraseButton") ;
		
		// destroy all the characters and projectiles
		for( int i=0 ; i<players.Length ; i++ )
			Destroy (players[i]) ;
		
		for( int j=0 ; j<enemies.Length ; j++ )
			Destroy (enemies[j]) ;
		
		for( int k=0 ; k<projectiles.Length ; k++ )
			Destroy (projectiles[k]) ;
		for( int k=0 ; k<projectiles2.Length ; k++ )
			Destroy (projectiles2[k]) ;
		
		// reset all the tiles
		for( int n=0 ; n<tiles.Length ; n++ )
			tiles[n].gameObject.GetComponent<TileTaken>().isTaken = false ;

		eraseButton.gameObject.GetComponent<EraserScript>().activeEraser = false ;

		// restore the inicial value for the score
		this.gameObject.GetComponent<PlayerScore>().score = this.initialScore ;
		
		// restore the cooldown for enemy spaw
		this.gameObject.GetComponent<EnemyWaveManager>().cooldownSpawn = this.inicialCooldown ;
		
		// restart the wave of enemies
		this.gameObject.GetComponent<EnemyWaveManager>().numEnemiesSpawned = 0 ;

		// restart the game
		this.end = false ;
		this.won = false ;
	}
}
