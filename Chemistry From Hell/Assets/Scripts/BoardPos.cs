﻿using UnityEngine;
using System.Collections;

public class BoardPos {

	public float xPos { get; set; }
	public float yPos { get; set; }
	public float zPos { get; set; }

	private Vector3 Pos ;
	
	public BoardPos( float x , float y )
	{
		this.yPos = y;
		this.xPos = x;
		this.zPos = -39f;

		this.Pos = new Vector3 (this.xPos, this.yPos, this.zPos);
	}

	public Vector3 getVector3()
	{
		return this.Pos;
	}
}
